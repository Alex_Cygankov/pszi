﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Collections.Generic;
using System.Linq;
using System.Collections;

namespace PSZI
{
    static class BigSimpleIntGenerator
    {
        private const int UP_LIMIT = 1000000000;
        private static List<uint> smallSimples;
        private static int[] lotsOfSimples;
        private static RNGCryptoServiceProvider rnd;
        public static int iterations; // количество итераций последнего вызова метода GetBigSimple

        static BigSimpleIntGenerator()
        {
            uint number = 2;
            lotsOfSimples = null;
            rnd = new RNGCryptoServiceProvider();
            smallSimples = new List<uint>();
            while (number < 2000)
            {
                bool isSimple = true;
                for (int i = 0; i < smallSimples.Count && isSimple; i++)
                {
                    isSimple = number % smallSimples[i] != 0;
                }
                if (isSimple)
                {
                    smallSimples.Add(number);
                }
                else
                {
                    isSimple = true;
                }
                number++;
            }
        }

        public static BigInteger GetBigRandomSimple(int sizeInBits, int checksCount)
        {
            int sizeInBytes = (sizeInBits + 7) / 8;
            byte[] buffer = new byte[sizeInBytes];
            BigInteger bigSimple;
            bool isSimple;
            iterations = 0;

            do
            {
                isSimple = true;
                buffer = (GetBigRandom(sizeInBits)); //генерируем много рандомных бит

                buffer[sizeInBytes - 1] |= (byte)Math.Pow(2, (sizeInBits - 1) % 8); // ставим 1 в последний бит
                buffer[0] |= 1; // ставим 1 в первый бит
                bigSimple = new BigInteger(buffer);

                // проверяем есть ли делители среди простых часел до 2000
                for (int i = 0; isSimple && i < smallSimples.Count; i++)
                {
                    isSimple = bigSimple % smallSimples[i] != 0;
                }

                // Тест Рабина-Миллера
                if (isSimple)
                {
                    isSimple = Rabin_MillerTest(bigSimple, sizeInBits, checksCount);
                }

                iterations++;
            } while (!isSimple);

            return bigSimple;
        }

        public static int GetGratestPowOfTwo(BigInteger value)
        {
            int pow = 0;
            BigInteger tmp = value - 1;
            while (tmp % 2 == 0)
            {
                pow++;
                tmp /= 2;
            }
            return pow;
        }

        public static byte[] GetBigRandom(int sizeInBits)
        {
            int sizeInBytes = (sizeInBits + 7) / 8;
            byte[] buffer = new byte[sizeInBytes];

            rnd.GetBytes(buffer); // генерируем случайное большое число
            for (byte i = 128; i >= Math.Pow(2, sizeInBits % 8); i /= 2) // мудреный способ отсечь лишние биты
            {
                buffer[sizeInBytes - 1] &= (byte)(i ^ 0xFF);
            }
            return buffer;
        }

        public static bool IsSimple(BigInteger big, int sizeInBits, int checksCount)
        {
            bool isSimple = true;
            // проверяем есть ли делители среди простых часел до 2000
            for (int i = 0; isSimple && i < smallSimples.Count; i++)
            {
                isSimple = big % smallSimples[i] != 0;
            }

            // Тест Рабина-Миллера
            if (isSimple)
            {
                isSimple = Rabin_MillerTest(big, sizeInBits, checksCount);
            }
            return isSimple;
        }

        public static int GetSizeInBits(BigInteger big)
        {
            byte[] bytes= big.ToByteArray();
            int size = bytes.Length * 8;
            byte tmp = 0b10000000;
            while ((tmp & bytes.Last()) != tmp)
            {
                size--;
                tmp /= 2;
            }
            return size;
        }


        public static bool Rabin_MillerTest(BigInteger p, int sizeInBits, int checksCount)
        {
            //int pow = 1;
            BigInteger m;
            BigInteger z;
            BigInteger b = 0;
            BigInteger tmp = p - 1;

            while (tmp % 2 == 0)
            {
                b++;
                tmp /= 2;
                //pow *= 2;
            }
            m = tmp;

            for (int i = 0; i < checksCount; i++)
            {
                BigInteger a = new BigInteger(p.ToByteArray());
                bool isChecked = false;

                while (a >= p)// 1 пункт
                {
                    a = new BigInteger(GetBigRandom(sizeInBits));
                }
                z = BigInteger.ModPow(a, m, p);// 2 пункт

                isChecked = z == 1 || z == (p - 1); // 3 пункт
                for (int j = 1;  !isChecked && j < b; j++)// 5 пункт
                {
                    if (z < (p - 1))
                    {
                        z = BigInteger.ModPow(z, 2, p);
                        if (z == 1)
                        {
                            return false;// 4 пункт
                        } 
                    }
                    else if (z == (p - 1))
                    {
                        isChecked = true;
                    }
                }
                if (!isChecked && z != (p - 1))
                {
                    return false;// 6 пункт
                }
            }
            return true;
        }

        public static BigInteger[] GetBigSimplesInRange(BigInteger from, BigInteger to, int sizeInBits)
        {
            List<BigInteger> simples = new List<BigInteger>();
            bool isSimple;
            for (BigInteger i = from; i <= to; i++)
            {
                isSimple = true;
                // проверяем есть ли делители среди простых часел до 2000
                for (int k = 0; isSimple && k < smallSimples.Count; k++)
                {
                    isSimple = i % smallSimples[k] != 0;
                }

                if (isSimple && Rabin_MillerTest(i, sizeInBits, 5))
                {
                    simples.Add(i);
                }
            }
            return simples.ToArray();
        }

        public static BigInteger[] GetSimpleDeviders(BigInteger simple)
        {
            List<BigInteger> deviders = new List<BigInteger>();
            BigInteger p = simple - 1;

            foreach (var smallSimple in smallSimples)
            {
                if (p % smallSimple == 0)
                {
                    deviders.Add(smallSimple);
                    do
                    {
                        p /= smallSimple;
                    }
                    while(p % smallSimple == 0); 
                }
            }

            for (BigInteger probe = deviders.Last(); probe * probe < p; probe++) //обновляем верхнюю границу перебора
            {
                if (p % probe == 0)
                {
                    deviders.Add(probe);
                    do
                    {
                        p /= probe;
                    }
                    while (p % probe == 0);
                }
            }
            return deviders.ToArray();
        }

        public static BigInteger[] GetRoots(BigInteger simple, int count)
        {
            List<BigInteger> roots = new List<BigInteger>();
            BigInteger p = simple - 1;
            BigInteger[] deviders = GetSimpleDeviders(simple);
            BigInteger[] degrees = new BigInteger[deviders.Count()];
            BigInteger num = 2;

            for (int i = 0; i < deviders.Count(); i++)
            {
                degrees[i] = p / deviders[i];
            }

            while (roots.Count < count && num < p)
            {
                bool isRoot = true;
                for (int i = 0; isRoot && i < deviders.Count(); i++)
                {
                    isRoot = BigInteger.ModPow(num, degrees[i], simple) != 1;
                }
                if (isRoot)
                {
                    roots.Add(num);
                }
                num++;
            }

            return roots.ToArray();
        }

        public static int[] GetLotsOfSimples()
        {
            if (lotsOfSimples == null)
            {
                SortedSet<int> simples = new SortedSet<int>();
                BitArray isSimple = new BitArray(5000, true);// от 2 до 2^31 - 2
                for (int i = 0; i < isSimple.Count; i++)
                {
                    if (isSimple[i])
                    {
                        simples.Add(i + 2);
                        for (int j = i; j < isSimple.Count; j += 2 + i)
                        {
                            isSimple[j] = false;
                        }
                    }
                }
                lotsOfSimples = simples.ToArray();
            }
            return lotsOfSimples;
        }
    }
}
