﻿using System;
using System.Numerics;
using System.Text;

namespace PSZI
{
    public static class RSA
    {

        // Шифрование сообщения M ассиметричным ключом
        public static byte[] Encode(byte[] M, BigInteger e, BigInteger n)
        {
            byte[] result;
            int k = Convert.ToInt32(Math.Floor(BigInteger.Log(n, 2.0)));
            int nUp = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2.0)));
            nUp += 8 - (nUp % 8);
            nUp /= 8;
            result = new byte[(M.Length + nUp - 1) / nUp * nUp];
            for (int i = 0; i < M.Length; i += nUp)
            {
                byte[] bb = new byte[Math.Min(nUp, M.Length - i)];
                for (int j = 0, m = i; j < Math.Min(nUp, M.Length - i); j++, m++)
                {
                    bb[j] = M[m];
                }
                BigInteger num = new BigInteger(bb);

                BigInteger c = BigInteger.ModPow(num, e, n);
                c.ToByteArray().CopyTo(result, nUp * i);
            }
            return result;
        }

        // Расшифрование сообщения M ассиметричным ключом
        public static byte[] Decode(byte[] C, BigInteger d, BigInteger n)
        {
            byte[] result;
            int nUp = Convert.ToInt32(Math.Ceiling(BigInteger.Log(n, 2.0)));
            nUp += 8 - (nUp % 8);
            StringBuilder res = new StringBuilder();

            nUp /= 8;
            result = new byte[(C.Length + nUp - 1) / nUp * nUp];
            for (int i = 0; i < C.Length; i += nUp)
            {
                byte[] bb = new byte[Math.Min(nUp, C.Length - i)];
                for (int j = 0, k = i; j < Math.Min(nUp, C.Length - i); j++, k++)
                {
                    bb[j] = C[k];
                }
                BigInteger num = new BigInteger(bb);

                BigInteger m = BigInteger.ModPow(num, d, n);
                m.ToByteArray().CopyTo(result, nUp * i);
            }
            return result;
        }

        // Подсчет функции Эйлера
        private static BigInteger euler(BigInteger n)
        {
            BigInteger result = n;
            if (BigSimpleIntGenerator.IsSimple(result, BigSimpleIntGenerator.GetSizeInBits(result), 5))
                return result - 1;
            for (BigInteger i = 2; i * i <= n; ++i)
            {
                if (n % i == 0)
                {
                    while (n % i == 0)
                        n /= i;
                    result -= result / i;
                }
            }
            if (n > 1)
                result -= result / n;
            return result;
        }


        static public BigInteger Euclid(BigInteger a, BigInteger b)
        {
            BigInteger u = b, tmp, q, r;
            BigInteger[][] E = new BigInteger[][] { new BigInteger[] { 1, 0 }, new BigInteger[] { 0, 1 } };
            do
            {
                q = BigInteger.DivRem(a, b, out r);
                if (r == 0)
                {
                    while (E[0][1] < 0)
                        E[0][1] += u;
                    return E[0][1];
                }
                tmp = E[0][1];
                E[0][1] = E[0][0] - q * E[0][1];
                E[0][0] = tmp;
                tmp = E[1][1];
                E[1][1] = E[1][0] - q * E[1][1];
                E[1][0] = tmp;
                a = b;
                b = r;
            } while (true);
        }



    }
}
