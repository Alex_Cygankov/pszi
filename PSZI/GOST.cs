﻿using System;
using System.Collections;

namespace PSZI
{
    public class GOST
    {
        const int KEY_SIZE = 32;

        private byte[,] SBlocks =
        {
            { 4, 10, 9, 2, 13, 8, 0, 14, 6, 11, 1, 12, 7, 15, 5, 3},
            { 14, 11, 4, 12, 6, 13, 15, 10, 2, 3, 8, 1, 0, 7, 5, 9},
            { 5, 8, 1, 13, 10, 3, 4, 2, 14, 15, 12, 7, 6, 0, 9, 11},
            { 7, 13, 10, 1, 0, 8, 9, 15, 14, 4, 6, 12, 11, 2, 5, 3},

            { 6, 12, 7, 1, 5, 15, 13, 8, 4, 10, 9, 14, 0, 3, 11, 2},
            { 4, 11, 10, 0, 7, 2, 1, 13, 3, 6, 8, 5, 9, 12, 15, 14},
            { 13, 11, 4, 1, 3, 15, 5, 9, 0, 10, 14, 7, 6, 8, 2, 12},
            { 1, 15, 13, 0, 5, 7, 10, 4, 9, 2, 3, 14, 6, 11, 8, 12}
        };

        private int[] raundKeysOrder =
        {
            0, 1, 2, 3, 4, 5, 6, 7,
            0, 1, 2, 3, 4, 5, 6, 7,
            0, 1, 2, 3, 4, 5, 6, 7,
            7, 6, 5, 4, 3, 2, 1, 0
        };

        private bool[] key;

        public bool[] Key
        {
            get => (bool[])key.Clone();
            set
            {
                if (value != null)
                {
                    if (value.Length == 32)
                    {
                        key = (bool[])value.Clone();
                    }
                    else
                    {
                        throw new Exception("Invalid key size.");
                    }
                }
                else
                {
                    key = null;
                }
            }
        }

        public GOST(bool[] key)
        {
            if (key == null)
            {
                throw new NullReferenceException(); 
            }
            if (key.Length != 256)
            {
                throw new ArgumentOutOfRangeException();
            }
            this.key = key;
        }

        private BitArray GetRaundKey(int raundIndex)
        {
            bool[] bits = new bool[32];
            for (int i = 0, j = raundKeysOrder[raundIndex] * 32; i < 32; i++, j++)
            {
                bits[i] = key[j];
            }
            return new BitArray(bits);
        }

        private byte BitsToInt(bool[] bits)
        {
            byte result = 0;
            for (int i = 0; i < bits.Length; i++)
                result = (byte)(result * 2 + (bits[i] ? 1 : 0));
            return result;
        }

        public BitArray Encode(BitArray data)
        {
            var blocks = SplitIntoBlocks(data);
            BitArray result = new BitArray(blocks.GetLength(0) * 64);

            for (int i = 0, offset = 0; i < blocks.GetLength(0); i++, offset += 64)
            {
                int leftHalfIndex = 0;
                int rightHalfIndex = 1;
                for (int raund = 0; raund < 32; raund++)
                {
                    BitArray raundResult = GetRaundKey(raund);
                    BitArray temp;

                    raundResult.Xor(blocks[i, rightHalfIndex]);

                    for (int s = 0; s < 8; s++)
                    {
                        bool[] oldVal = new bool[4];
                        for ( int k = 0, l = s*4; k < 4; k++, l++)
                        {
                            oldVal[k] = raundResult[l];
                        }
                        byte newVal = SBlocks[s, BitsToInt(oldVal)];
                        raundResult.Set(s * 4, (newVal & 0b00001000) == 0b00001000);
                        raundResult.Set(s * 4 + 1, (newVal & 0b00000100) == 0b00000100);
                        raundResult.Set(s * 4 + 2, (newVal & 0b00000010) == 0b00000010);
                        raundResult.Set(s * 4 + 3, (newVal & 0b00000001) == 0b00000001);
                    }

                    temp = new BitArray(raundResult);
                    for (int j = 0; j < 32; j++)
                    {
                        raundResult.Set(j, temp.Get((11 + j) % 32));
                    }

                    blocks[i, leftHalfIndex].Xor(raundResult);
                    leftHalfIndex = (raund + 1) % 2;
                    rightHalfIndex = raund % 2;
                }

                for (int j = 0; j < 32; j++)
                {
                    result[offset + j] = blocks[i, 1][j];
                    result[offset + j + 32] = blocks[i, 0][j];
                }
            }
            return result;
        }

        public BitArray Decode(BitArray data)
        {
            var blocks = SplitIntoBlocks(data);
            BitArray result = new BitArray(blocks.GetLength(0) * 64);
            for (int i = 0, offset = 0; i < blocks.GetLength(0); i++, offset += 64)
            {
                int leftHalfIndex = 1;
                int rightHalfIndex = 0;
                for (int raund = 31; raund >= 0; raund--)
                {
                    BitArray raundResult = new BitArray(blocks[i, leftHalfIndex]);
                    BitArray temp;

                    raundResult.Xor(GetRaundKey(raund));

                    for (int s = 0; s < 8; s++)
                    {
                        bool[] oldVal = new bool[4];
                        for (int k = 0, l = s * 4; k < 4; k++, l++)
                        {
                            oldVal[k] = raundResult[l];
                        }
                        byte newVal = SBlocks[s, BitsToInt(oldVal)];
                        raundResult.Set(s * 4, (newVal & 0b00001000) == 0b00001000);
                        raundResult.Set(s * 4 + 1, (newVal & 0b00000100) == 0b00000100);
                        raundResult.Set(s * 4 + 2, (newVal & 0b00000010) == 0b00000010);
                        raundResult.Set(s * 4 + 3, (newVal & 0b00000001) == 0b00000001);
                    }

                    temp = new BitArray(raundResult);
                    for (int j = 0; j < 32; j++)
                    {
                        raundResult.Set(j, temp.Get((11 + j) % 32));
                    }

                    blocks[i, rightHalfIndex].Xor(raundResult);
                    leftHalfIndex = (raund + 1) % 2;
                    rightHalfIndex = raund % 2;
                }

                for (int j = 0; j < 32; j++)
                {
                    result[offset + j] = blocks[i, leftHalfIndex][j];
                    result[offset + j + 32] = blocks[i, rightHalfIndex][j];
                }
            }
            return result;
        }

        public BitArray[] GetAvalanche(BitArray data)
        {
            BitArray[] raunds = new BitArray[32];
            var blocks = SplitIntoBlocks(data);
            BitArray result = new BitArray(64);

            int leftHalfIndex = 0;
            int rightHalfIndex = 1;
            for (int raund = 0; raund < 32; raund++)
            {
                BitArray raundResult = GetRaundKey(raund);
                BitArray temp;

                raundResult.Xor(blocks[0, rightHalfIndex]);

                for (int s = 0; s < 8; s++)
                {
                    bool[] oldVal = new bool[4];
                    for (int k = 0, l = s * 4; k < 4; k++, l++)
                    {
                        oldVal[k] = raundResult[l];
                    }
                    byte newVal = SBlocks[s, BitsToInt(oldVal)];
                    raundResult.Set(s * 4, (newVal & 0b00001000) == 0b00001000);
                    raundResult.Set(s * 4 + 1, (newVal & 0b00000100) == 0b00000100);
                    raundResult.Set(s * 4 + 2, (newVal & 0b00000010) == 0b00000010);
                    raundResult.Set(s * 4 + 3, (newVal & 0b00000001) == 0b00000001);
                }

                temp = new BitArray(raundResult);
                for (int j = 0; j < 32; j++)
                {
                    raundResult.Set(j, temp.Get((11 + j) % 32));
                }

                blocks[0, leftHalfIndex].Xor(raundResult);
                leftHalfIndex = (raund + 1) % 2;
                rightHalfIndex = raund % 2;

                raunds[raund] = new BitArray(64);
                for (int j = 0; j < 32; j++)
                {
                    raunds[raund][j] = blocks[0, leftHalfIndex][j];
                    raunds[raund][j + 32] = blocks[0, rightHalfIndex][j];
                }
            }
            for (int j = 0; j < 32; j++)
            {
                raunds[31][j] = blocks[0, rightHalfIndex][j];
                raunds[31][j + 32] = blocks[0, leftHalfIndex][j];
            }
            return raunds;
        }

        public double[] GetAvalancheCriteria(BitArray X)
        {
            if (X == null)
            {
                return null; 
            }
            double[] d = new double[4];
            BitArray[,] U1 = SplitIntoBlocks(X);
            BitArray Y = Encode(X);
            BitArray Xi = new BitArray(X);
            BitArray[] Yi = new BitArray[64];
            BitArray[] dY = new BitArray[64];
            int[] wdY = new int[6];
            int[,] a = new int[64, 64];
            int[,] b = new int[64, 64];

            for (int i = 0; i < 64; i ++)
            {
                Xi[i] = !Xi[i];
                Yi[i] = Encode(Xi);
                dY[i] = new BitArray(Y).Xor(Yi[i]);
                for(int j = 0; j < 64; j++)
                { 

                }
                Xi[i] = !Xi[i];
            }

            return d;
        }

        public static BitArray[,] SplitIntoBlocks(BitArray data)
        {
            if (data != null)
            {
                int blockCount = (data.Length + 63) / 64;
                int offset = 0;
                BitArray[,] blocks = new BitArray[blockCount, 2];
                for (int i = 0; i < blockCount - 1; i++)
                {
                    blocks[i, 0] = new BitArray(32);
                    blocks[i, 1] = new BitArray(32);
                    offset = i * 64;
                    for (int j = 0; j < 32; j++)
                    {
                        blocks[i, 0][j] = data[offset + j];
                        blocks[i, 1][j] = data[offset + 32 + j];
                    }
                }

                blocks[blockCount - 1, 0] = new BitArray(32);
                blocks[blockCount - 1, 1] = new BitArray(32);
                offset = (blockCount - 1) * 64;
                int voidBits = blockCount * 64 - data.Length;
                if (voidBits <= 32)
                {
                    for (int j = 0; j < 32; j++)
                    {
                        blocks[blockCount - 1, 0][j] = data[offset + j];
                    }
                    for (int j = 0; j < 32 - voidBits; j++)
                    {
                        blocks[blockCount - 1, 1][j] = data[offset + 32 + j];
                    }
                }
                else
                {
                    for (int j = 0; j < 64 - voidBits; j++)
                    {
                        blocks[blockCount - 1, 0][j] = data[offset + j];
                    }
                }
                return blocks;
            }
            throw new Exception("Empty data to encode.");
        }

    }
}