﻿using System;
using System.Numerics;
using System.Security.Cryptography;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Collections;
using System.IO;

namespace PSZI
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        RNGCryptoServiceProvider rnd = new RNGCryptoServiceProvider();
        public BitArray bitArray;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonFillPQ_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                switch (TabSignature.SelectedIndex)
                {
                    case 0:
                        {
                            int sizeInBits = int.Parse(InputSizeInBits.Text);
                            int checksCount = int.Parse(InputChecksCount.Text);
                            BigInteger p;
                            p = BigSimpleIntGenerator.GetBigRandomSimple(sizeInBits, checksCount);
                            InputElGamalP.Text = p.ToString();
                            break;
                        }
                    case 1:
                        {
                            int sizeInBits = int.Parse(InputSizeInBits.Text);
                            int checksCount = int.Parse(InputChecksCount.Text);
                            BigInteger p;
                            BigInteger q;
                            p = BigSimpleIntGenerator.GetBigRandomSimple(sizeInBits, checksCount);
                            q = BigSimpleIntGenerator.GetBigRandomSimple(sizeInBits, checksCount);
                            InputRSAP.Text = p.ToString();
                            InputRSAQ.Text = q.ToString();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ButtonGetRSAKeys_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int inputChecks = 0;
                string problems = "";
                int[] sizeInBits = new int[2];
                int checksCount = int.Parse(InputChecksCount.Text);
                switch (TabSignature.SelectedIndex)
                {
                    case 0:
                        {
                            BigInteger p = BigInteger.Parse(InputElGamalP.Text),
                                g, x, y;
                            int rndIndx;
                            BigInteger[] roots;
                            byte[] buf = new byte[4];
                            sizeInBits[0] = BigSimpleIntGenerator.GetSizeInBits(p);
                            if (!BigSimpleIntGenerator.IsSimple(p, sizeInBits.Max(), 5))
                            {
                                problems += " - p не простое число\n";
                                inputChecks--;
                            }
                            if (inputChecks == 0)
                            {
                                roots = BigSimpleIntGenerator.GetRoots(p, 100);
                                rnd.GetBytes(buf);
                                rndIndx = Math.Abs(BitConverter.ToInt32(buf, 0)) / roots.Length;
                                g = roots[rndIndx];
                                do
                                {
                                    x = new BigInteger(BigSimpleIntGenerator.GetBigRandom(sizeInBits[0]));
                                }
                                while (x >= p);
                                y = BigInteger.ModPow(g, x, p);
                                OutputElGamalY.Text = y.ToString();
                                OutputElGamalG.Text = g.ToString();
                                OutputElGamalX.Text = x.ToString();

                            }
                            else
                            {
                                MessageBox.Show(problems);
                            }
                            break;
                        }
                    case 1:
                        {
                            BigInteger p = BigInteger.Parse(InputRSAP.Text);
                            sizeInBits[0] = BigSimpleIntGenerator.GetSizeInBits(p);
                            BigInteger q = BigInteger.Parse(InputRSAQ.Text);
                            sizeInBits[1] = BigSimpleIntGenerator.GetSizeInBits(q);
                            InputRSAP.Text = p.ToString();
                            InputRSAQ.Text = q.ToString();
                            if (!BigSimpleIntGenerator.IsSimple(p, sizeInBits.Max(), 5))
                            {
                                problems += " - p не простое число\n";
                                inputChecks--;
                            }
                            if (!BigSimpleIntGenerator.IsSimple(q, sizeInBits.Max(), 5))
                            {
                                problems += " - q не простое число\n";
                                inputChecks--;
                            }
                            if (inputChecks == 0)
                            {
                                BigInteger n = p * q;
                                BigInteger fn = (p - 1) * (q - 1);
                                BigInteger veryBigRandom;
                                BigInteger d;
                                int fnSizeInBits = BigSimpleIntGenerator.GetSizeInBits(fn);
                                do
                                {
                                    veryBigRandom = BigSimpleIntGenerator.GetBigRandomSimple(fnSizeInBits, 5);
                                }
                                while (veryBigRandom >= fn);

                                d = RSA.Euclid(veryBigRandom, fn);

                                OutputRSAN.Text = n.ToString();
                                OutputRSAOpenKey.Text = veryBigRandom.ToString();
                                OutputRSACloseKey.Text = d.ToString();
                            }
                            else
                            {
                                MessageBox.Show(problems);
                            }
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private byte[] GetHash(byte[] data)
        {
            using (MD5 md5 = MD5.Create())
            {
                return md5.ComputeHash(data);
            }
        }

        private byte[] GetHash(Stream stream)
        {
            using (MD5 md5 = MD5.Create())
            {
                return md5.ComputeHash(stream);
            }
        }

        private void ButtonGetSignature_Click(object sender, RoutedEventArgs e)
        {
            if (OutputFileName.Tag == null)
            {
                MessageBox.Show("Файл не выбран");
            }
            else
            {
                FileInfo fileInfo = (FileInfo)OutputFileName.Tag;
                switch (TabSignature.SelectedIndex)
                {
                    case 0:
                        {
                            BigInteger p = BigInteger.Parse(InputElGamalP.Text),
                                g = BigInteger.Parse(OutputElGamalG.Text),
                                x = BigInteger.Parse(OutputElGamalX.Text),
                                m = new BigInteger(GetHash(fileInfo.OpenRead())),
                                k, a, b;
                            OutputLog.Text += string.Format("вычисление хэш-функции");
                            OutputLog.Text += string.Format("m = {0}\n", m);
                            do
                            {
                                OutputLog.Text += "генерация k...\n";
                                k = BigSimpleIntGenerator.GetBigRandomSimple(BigSimpleIntGenerator.GetSizeInBits(p), 5);
                            }
                            while (k % (p - 1) == 0 || (p - 1) % k == 0);
                            OutputLog.Text += string.Format("k = {0}\n", k);
                            a = BigInteger.ModPow(g, k, p);
                            OutputLog.Text += string.Format("a = {0}\n", a);
                            b = RSA.Euclid(k, p - 1) * (m - x * a);
                            OutputLog.Text += string.Format("b = {0}\n", b);
                            OutputElGamalMD5.Text = m.ToString();
                            OutputElGamalK.Text = k.ToString();
                            OutputElGamalA.Text = a.ToString();
                            OutputElGamalB.Text = b.ToString();
                            break;
                        }
                    case 1:
                        {
                            BigInteger n = BigInteger.Parse(OutputRSAN.Text),
                                d = BigInteger.Parse(OutputRSACloseKey.Text),
                                m = new BigInteger(GetHash(fileInfo.OpenRead())),
                                s;
                            OutputLog.Text += string.Format("вычисление хэш-функции");
                            OutputLog.Text += string.Format("m = {0}\n", m);
                            s = BigInteger.ModPow(m, d, n);
                            OutputLog.Text += string.Format("s = {0}\n", s);
                            OutputRSAMD5.Text = m.ToString();
                            OutputRSAS.Text = s.ToString();
                            break;
                        }
                    default:
                        {
                            break;
                        }
                }
            }
        }

        private void ButtonCheckSignature_Click(object sender, RoutedEventArgs e)
        {
            switch (TabSignature.SelectedIndex)
            {
                case 0:
                    {
                        BigInteger p = BigInteger.Parse(InputElGamalP.Text),
                            y = BigInteger.Parse(OutputElGamalY.Text),
                            a = BigInteger.Parse(OutputElGamalA.Text), 
                            b = BigInteger.Parse(OutputElGamalB.Text),
                            left, right;
                        left = BigInteger.ModPow(BigInteger.ModPow(y, a, p) * BigInteger.ModPow(a, b, p), 1, p);
                        OutputLog.Text += string.Format("y^a * a^b mod p = {0}\n", left);
                        right = BigInteger.ModPow(BigInteger.ModPow(y, a, p) * BigInteger.ModPow(a, b, p), 1, p);
                        OutputLog.Text += string.Format("g^m mod p = {0}\n", right);
                        OutputLog.Text += left == right
                            ? string.Format("Подпись действительна\n")
                            : string.Format("Подпись не действительна\n");
                        MessageBox.Show(left == right
                            ? string.Format("Подпись действительна\n")
                            : string.Format("Подпись не действительна\n"));
                        break;
                    }
                case 1:
                    {
                        BigInteger n = BigInteger.Parse(OutputRSAN.Text),
                            openKey = BigInteger.Parse(OutputRSAOpenKey.Text),
                            m = BigInteger.Parse(OutputRSAMD5.Text),
                            s = BigInteger.Parse(OutputRSAS.Text),
                            h;
                        h = BigInteger.ModPow(s, openKey, n);
                        OutputLog.Text += string.Format("h(M) = {0}\n", h);
                        OutputLog.Text += h == m 
                            ? string.Format("Подпись действительна\n")
                            : string.Format("Подпись не действительна\n");
                        MessageBox.Show(h == m
                            ? string.Format("Подпись действительна\n")
                            : string.Format("Подпись не действительна\n"));
                        break;
                    }
                default:
                    {
                        break;
                    }
            }
        }

        private void ButtonExplore_Click(object sender, RoutedEventArgs e)
        {
            using (var ofd = new System.Windows.Forms.OpenFileDialog())
            {
                if (ofd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    FileInfo fi = new FileInfo(ofd.FileName);
                    OutputFileName.Text = fi.Name;
                    OutputFileName.Tag = fi;
                }
            }
        }
    }
}
